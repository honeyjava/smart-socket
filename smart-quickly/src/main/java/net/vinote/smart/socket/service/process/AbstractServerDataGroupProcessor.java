package net.vinote.smart.socket.service.process;

import net.vinote.smart.socket.transport.IoSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 服务器消息处理器,由服务器启动时构造
 *
 * @author Seer
 */
public abstract class AbstractServerDataGroupProcessor<T> implements ProtocolDataProcessor<T> {
    private static Logger logger = LogManager.getLogger(AbstractServerDataGroupProcessor.class);
    public static final String SESSION_PROCESS_THREAD = "_PROCESS_THREAD_";
    /**
     * 消息处理线程
     */
    private ServerDataProcessThread[] processThreads;

    private AtomicInteger processThreadIndex = new AtomicInteger(0);

    @SuppressWarnings("unchecked")
    @Override
    public void init(int threadNum) {
        // 启动线程池处理消息
        processThreads = new AbstractServerDataGroupProcessor.ServerDataProcessThread[threadNum];
        for (int i = 0; i < processThreads.length; i++) {
            processThreads[i] = new ServerDataProcessThread("ServerProcess-Thread-" + i);
            processThreads[i].setPriority(Thread.MAX_PRIORITY);
            processThreads[i].start();
        }
    }

    @Override
    public boolean receive(IoSession<T> session, T entry) {
        ProcessUnit unit = new ProcessUnit(session, entry);
        ServerDataProcessThread processThread = session.getAttribute(SESSION_PROCESS_THREAD);
        //当前Session未绑定处理器,则先进行处理器选举
        if (processThread == null) {
            processThread = processThreads[processThreadIndex.getAndIncrement() % processThreads.length];
            session.setAttribute(SESSION_PROCESS_THREAD, processThread);
        }
        if (processThread.msgQueue.offer(unit)) {
            return true;
        }
        //Session绑定的处理器处理能力不足，由其他处理器辅助
//        for (int i = processThreads.length - 1; i >= 0; i--) {
//            if (processThreads[i].msgQueue.offer(unit)) {
//                return true;
//            }
//        }
        //所有线程都满负荷
        try {
            processThread.msgQueue.put(unit);
        } catch (InterruptedException e) {
            logger.catching(e);
        }

        return true;
    }

    @Override
    public void shutdown() {
        if (processThreads != null && processThreads.length > 0) {
            for (ServerDataProcessThread thread : processThreads) {
                thread.shutdown();
            }
        }
    }

    /**
     * 消息数据元
     *
     * @author zhengjunwei
     */
    final class ProcessUnit {
        IoSession<T> session;
        T msg;

        public ProcessUnit(IoSession<T> session, T msg) {
            this.session = session;
            this.msg = msg;
        }
    }


    /**
     * 服务端消息处理线程
     *
     * @author zhengjunwei
     */
    final class ServerDataProcessThread extends Thread {
        private volatile boolean running = true;

        public ServerDataProcessThread(String name) {
            super(name);
        }

        /**
         * 消息缓存队列
         */
        private ArrayBlockingQueue<ProcessUnit> msgQueue = new ArrayBlockingQueue<ProcessUnit>(4096);

        @Override
        public void run() {
            while (running) {
                try {
                    ProcessUnit unit = msgQueue.take();
                    unit.session.getFilterChain().doProcessFilter(unit.session, unit.msg);
                    if (unit.session.isValid()) {
                        AbstractServerDataGroupProcessor.this.process(unit.session, unit.msg);
                    } else {
                        if (logger.isInfoEnabled()) {
                            logger.info("session invliad,discard message:" + unit.msg);
                        }
                        System.out.println(unit.session.getStatus());
                        System.exit(0);
                    }
                } catch (Exception e) {
                    if (running) {
                        logger.warn(e.getMessage(), e);
                    }
                }
            }
        }

        /**
         * 停止消息处理线程
         */
        public void shutdown() {
            running = false;
            this.interrupt();
        }
    }
}
