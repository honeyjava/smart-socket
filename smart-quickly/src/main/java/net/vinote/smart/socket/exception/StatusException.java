package net.vinote.smart.socket.exception;

/**
 * 状态异常
 * 
 * @author Seer
 * 
 */
public class StatusException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3598837824868397943L;

	public StatusException(String string) {
		super(string);
	}

}
