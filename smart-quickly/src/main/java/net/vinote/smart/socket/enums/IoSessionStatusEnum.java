package net.vinote.smart.socket.enums;

/**
 * 传输层通道状态
 * 
 * @author Seer
 * @version IoSessionStatusEnum.java, v 0.1 2015年3月21日 下午4:34:26 Seer Exp.
 */
public enum IoSessionStatusEnum {
	CLOSED, CLOSING, ENABLED,INVALID;
}
